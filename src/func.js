const getSum = (str1, str2) => {
  if (str1.length > 21 || str2.length > 21) {
    function resultFunc(a, b, result, curr, start) {
      if(a.length == 0 && b.length == 0 && !curr) {
        return result;
      }
        
      let left = parseInt(a.pop() || '0', 10);
      let right = parseInt(b.pop() || '0', 10);
    
      let l = left + right + (curr || 0);
    
      return resultFunc(a, b, l % start + (result || ""), Math.floor(l/start), start);
    }
    
    function add(a, b) {
      return resultFunc(a.toString().split(""), b.toString().split(""), "","", 10).toString();
    }

    return add(str1, str2);
  }

  const finalResult = +str1 + +str2;

  if (!finalResult) {
    return false
  }

  return '' + finalResult;
};


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter(post => post.author === authorName).length;

  const comments = listOfPosts
    .filter(post => post.comments)
    .map(post => post.comments
      .filter(comment => comment.author === authorName).length
    ).reduce((sum, current) => sum + current, 0);

  return `Post:${posts},comments:${comments}`;
};


const tickets= (people) => {
  let sum = 0;

  if (people[0] > 25) {

    return 'NO';
  }

  for (let i = 0; i < people.length; i++) {
    sum += 25;

    if (sum < +people[i + 1] - 25) {
      return 'NO';
    }
  }

  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
